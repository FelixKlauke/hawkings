# hawkings

Very simple and more or less useful framework to provide a local repository of local Consumers.

# Installation / Usage

- Install [Maven](http://maven.apache.org/download.cgi)
- Clone this repo
- Installh: ```mvn clean install```

**Maven dependencies**

_Hwakings:_
```xml
<dependency>
    <groupId>de.d3adspace</groupId>
    <artifactId>hawkings</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```
